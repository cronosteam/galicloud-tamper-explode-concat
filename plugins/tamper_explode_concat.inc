<?php

define('CONCAT_BEFORE', 0);
define('CONCAT_AFTER', 1);

/**
 * @file
 * Do super awesome thing.
 */
$plugin = array(
		'form' => 'galicloud_tamper_explode_concat_form',
		// Optional validation callback.
		'validate' => 'galicloud_tamper_explode_concat_validate',
		'callback' => 'galicloud_tamper_explode_concat_callback',
		'name' => 'Explode Concat',
		'multi' => 'direct',
		'category' => 'Other',
);
function galicloud_tamper_explode_concat_form($importer, $element_key, $settings) {
	 $form = array();
  $form['separator'] = array(
    '#type' => 'textfield',
  	'#required' => true,
  	'#title' => t('String separator'),
    '#default_value' => isset($settings['separator']) ? $settings['separator'] : ',',
    '#description' => t('This will break up sequenced data into an array. For example, "a, b, c" would get broken up into the array(\'a\', \'b\', \'c\').
                        A space can be represented by %s, tabs by %t, and newlines by %n.'),
  );
  $form['limit'] = array(
    '#type' => 'textfield',
    '#title' => t('Limit'),
    '#default_value' => isset($settings['limit']) ? $settings['limit'] : '',
    '#description' => t('If limit is set and positive, the returned items will contain a maximum of limit with the last item containing the rest of string.
                        If limit is negative, all components except the last -limit are returned. If the limit parameter is zero, then this is treated as 1. If limit is not set, then there will be no limit on the number of items returned.'),
  );
  $form['concat_string'] = array(
  		'#type' => 'textfield',
  		'#title' => t('Concat string'),
  		'#required' => true,
  		'#default_value' => isset($settings['concat_string']) ? $settings['concat_string'] : '',
  		'#description' => t('This will concat a custom string to each pieces resulting from the explode process'),
  );
  $form['concat_position'] = array(
  		'#type' => 'radios',
  		'#title' => t('Concat position'),
  		'#required' => true,
  		'#default_value' => isset($settings['concat_position']) ? $settings['concat_position'] : CONCAT_BEFORE,
  		'#description' => t('indicate the position to concat the string after or before the exploded token'),
  		'#options' => array(
  			CONCAT_BEFORE => t('Before'),
  			CONCAT_AFTER => t('After'),
  		),
  );
  
  
  return $form;
}
function galicloud_tamper_explode_concat_validate(&$settings) {
// Validate $settings.
}
function galicloud_tamper_explode_concat_callback($source, $item_key, $element_key, &$field, $settings) {
//$field = crazy_modification($field);
	
		
	if (!is_array($field)) {
		$field = array($field);
	}
	$out = array();
	foreach ($field as $f) {
		$exploded = array();
		if ($settings['limit'] === '') {
			$exploded = explode($settings['real_separator'], $f);			
		}
		else {
			$exploded = explode($settings['real_separator'], $f, $settings['limit']);
		}
		
		$explode_lenght = count($exploded);
		if($settings['concat_position'] == CONCAT_BEFORE){
			for ($i = 0; $i < $explode_length; $i ++) {
				$exploded[$i] = $settings['concat_string'] . $exploded[$i]; 
			}
		}else if($settings['concat_position'] == CONCAT_AFTER){
			for ($i = 0; $i < $explode_length; $i ++) {
				$exploded[$i] =  $exploded[$i] . $settings['concat_string'];
			}
		}
		
		
		$out = array_merge($out, $exploded);
	}
	$field = $out;


}


